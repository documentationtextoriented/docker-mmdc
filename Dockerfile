FROM node:14-buster-slim

LABEL maintainer="luc@mazardo.com"

RUN apt-get update && apt-get install -y ca-certificates gnupg2 wget --no-install-recommends \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-stable \
      --no-install-recommends \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /src/*.deb

RUN yarn add puppeteer @mermaid-js/mermaid-cli
COPY files/puppeteer-config.json /
COPY files/mmdc /usr/bin/

RUN rm -rf /node_modules/puppeteer
