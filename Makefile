CI_REGISTRY ?= registry.gitlab.com
CI_PROJECT_PATH ?= documentationtextoriented/mmdc

all: release

test: build
	@test -n "${CI_BUILD_TOKEN}" && docker run --rm -v $(PWD)/samples:/work test-documentationtextoriented/mmdc:latest mmdc -i /work/state.mermaid -o /work/state.png || true
	@test ! -n "${CI_BUILD_TOKEN}" && docker run --rm -v $(PWD)/samples:/work test-documentationtextoriented/mmdc:latest mmdc -i /work/state.mermaid -o /work/state.png || true
	echo "Test are ok"

build: login
	@docker build -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:latest .
	@docker build -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:buster-slim .
	@docker build -t test-${CI_PROJECT_PATH}:latest .
	echo "Builds are ok"

release: test
	# test "${CI_COMMIT_REF_NAME}" == "master" &&
	@test -n "${CI_BUILD_TOKEN}" && docker push ${REGISTRY_PATH} ${CI_REGISTRY}/${CI_PROJECT_PATH}:latest || true
	@test -n "${CI_BUILD_TOKEN}" && docker push ${REGISTRY_PATH} ${CI_REGISTRY}/${CI_PROJECT_PATH}:buster-slim || true

login:
	@test -n "${CI_BUILD_TOKEN}" && docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} ${CI_REGISTRY} || true

clean:
	@rm -f samples/state.png
