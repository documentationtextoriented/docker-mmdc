## Use it
```bash
docker run -it --rm -v $(pwd):/work mmdc -i /work/sample.mermaid -o /work/sample.png
```

## Build it
```bash
docker build -t mmdc:buster-slim .
```
